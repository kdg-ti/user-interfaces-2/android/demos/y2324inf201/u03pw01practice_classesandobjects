package be.kdg.ui2.event

enum class DayPart (val startTime:Int){
    NIGHT(0),
    MORNING(6),
    AFTERNOON(12),
    EVENING(18),
}
