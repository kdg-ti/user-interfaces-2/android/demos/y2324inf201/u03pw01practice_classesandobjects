package be.kdg.ui2.event

import be.kdg.ui2.event.DayPart.AFTERNOON

data class Event(var title:String, var description:String, var dayPart: DayPart, var duration: Int){
    companion object {
        var lunch = Event("lunch","noon meal", AFTERNOON,60)
    }
}
