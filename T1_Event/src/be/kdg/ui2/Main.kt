package be.kdg.ui2

import be.kdg.ui2.event.DayPart.*
import be.kdg.ui2.event.Event

fun Event.isShort(): Boolean {
    return this.duration < 60
}


fun main() {
    var study =
        Event("Study Kotlin", "Commit to studying Kotlin at least 6 hours per week", EVENING, 360)
    println("$study is ${if (study.isShort()) "" else "not"} short");
    println("$EVENING starts at ${EVENING.startTime} hours.")
    println("Coming soon: ${Event.lunch}")
}